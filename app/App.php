<?php

namespace HappsNowMiddleware\App;

use Nette\DI\Container;
use Tracy\Debugger;
use Nette\Utils\DateTime;
use DateTimeZone;
use Nette\Utils\FileSystem;
use Phar;
use PharData;

/**
 * Class App
 */
class App
{
    protected Container $container;

    /**
     *
     * @var string
     */
    protected $timezone;

    /**
     * App constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->timezone = new DateTimeZone('America/North_Dakota/Center');
    }

    /**
     *
     * @param string $module
     * @throws \ErrorException
     */
    public function run(string $module, ?string $par = null)
    {
        $apiConnector = $this->container->getService('happsConnector');
        $targetContainer = $this->container->getService('zohoConnector');
        $className = '\Zoho\Inserts\\' . $module;
        if (!class_exists($className)) {
            $message = sprintf('Class %s does not exists', $className);
            Debugger::log($message, 'error');
            throw new \ErrorException($message);
        }
        new $className($targetContainer, $apiConnector, $module, $par);
    }

    /**
     * Method generate refreshToken and store it into cache
     * @param $grandCode
     */
    public function generateToken($grandCode)
    {
        $oauthProvider = $this->container->getService('zohoOauthProvider');
        try {
            $oauthProvider->generateToken($grandCode);
            echo "Refresh token generated successfuly!";
        } catch (\Throwable $e) {
            echo $e->getMessage();
            Debugger::log($e, 'error');
        }
    }

    public function moveLogs()
    {
        $today = new DateTime('now', $this->timezone);
        $pattern = '/' . $today->format('Y-m-d') . '/';
        $files = array_diff(scandir(__LOG_DIR__), ['.', '..']);
        foreach ($files as $i => $file) {
            if (preg_match($pattern, $file)) {
                FileSystem::copy(__LOG_DIR__ . '/' . $file, __LOG_DIR__ . '/old/' . $file);
                FileSystem::delete(__LOG_DIR__ . '/' . $file);
            }
        }
    }

    public function packLogs()
    {
        $today = new DateTime('now - 4 days', $this->timezone);

        $pattern = '/' . $today->format('Y-m-d') . '/';
        $archive_file = realpath(__LOG_DIR__) . '/old/' . $today->format('Y-m-d') . '.tar';
        $files = array_diff(scandir(__LOG_DIR__), ['.', '..']);
        $files_to_pack = [];
        foreach ($files as $i => $file) {
            if (preg_match($pattern, $file)) {
                $files_to_pack[] = realpath(__LOG_DIR__) . '/' . $file;
            }
        }
        if (empty($files_to_pack)) {
            die('No Files to Pack for day: ' . $today->format('Y-m-d'));
        }
        $archive = new PharData($archive_file);
        foreach ($files_to_pack as $k => $v) {
            $archive->addFile($v);
            FileSystem::delete($v);
        }
        $archive->compress(Phar::GZ);
        FileSystem::delete($archive_file);

        die('end of packing for ' . $today->format('Y-m-d'));
    }
}
