<?php

set_time_limit(0);
define('__ROOT__', __DIR__ . '/../');
define('__TEMP_DIR__', __ROOT__ . '/temp');
define('__CACHE_DIR__', __ROOT__ . '/temp/cache');
define('__LOG_DIR__', __ROOT__ . 'log');
$autoLoader = require_once __ROOT__ . '/vendor/autoload.php';

/**
 *  debugger
 */
\Tracy\Debugger::enable(\Tracy\Debugger::DEVELOPMENT, __ROOT__ . '/log');

/**
 * class loading
 */
$loader = new \Nette\Loaders\RobotLoader();
$loader->addDirectory(__ROOT__ . '/app');
$loader->addDirectory(__ROOT__ . '/src');
$loader->setTempDirectory(__TEMP_DIR__);
$loader->register();


$di_loader = new Nette\DI\ContainerLoader(__TEMP_DIR__);

$class = $di_loader->load(function ($compiler) {
    if (file_exists(__DIR__ . '/config/config.local.neon')) {
        $compiler->loadConfig(__DIR__ . '/config/config.local.neon');
    }

    $compiler->loadConfig(__DIR__ . '/config/common.neon');
});


$app = new \HappsNowMiddleware\App\App(new $class);

if (php_sapi_name() === 'cli') {
    $argvCount = count($argv);
    if ($argvCount < 2) {
        throw new Exception('One parameter (module name) is required');
    }

    if ($argvCount == 2) {
        $module = $argv[1];
        $par = null;
        if (strpos($module, 'final')) {
            $module = str_replace('_final', '', $module);
            $par = 'final';
        }
        $app->run($module, $par);
    }

    if ($argvCount == 3) {
        switch ($argv[1]) {
            case 'generate_token':
            /* avoid to use Cammel Case in CLI */
            case 'generateToken':
                $app->generateToken($argv[2]);
                break;
            case 'logger':
                switch ($argv[2]) {
                    case 'pack':
                        $app->packLogs();
                        break;
                    case 'all':
                    default:
                        $app->moveLogs();
                }
                break;
            default: {
                    print 'Switch ' . $argv[1] . ' not supported';
                }
        }
    }
} else {
    //just for tests, do not use in production , use CLI instead
    //$module = 'partners';
    //$module = 'fee_structure';
    //$module = 'groups';
    //$module = 'promo_codes';
    //$module = 'events';
    //$module = 'transactions';
    //$module = 'tickets';
    //$module = 'ticket_types';
    //$module = 'customers';
    //$module = 'customers_x';
    //die();
    //end 31
    if (isset($_GET['module'])) {
        $module = $_GET['module'];
    } else {
        die('stop module is not set');
    }
    $par = null;
    if (strpos($module, 'final')) {
        $module = str_replace('_final', '', $module);
        $par = 'final';
    }
    $app->run($module, $par);
}

/**
 *
 * @param array $data
 * just for testing purposes
 */
function _print_d($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}
