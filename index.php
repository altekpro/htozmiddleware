<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

set_time_limit(0);
define('__ROOT__', __DIR__ . '/');
define('__TEMP_DIR__', __ROOT__ . '/temp');
define('__CACHE_DIR__', __ROOT__ . '/temp/cache');
$autoLoader = require_once __ROOT__ . '/vendor/autoload.php';

/**
 *  debugger
 */
\Tracy\Debugger::enable(\Tracy\Debugger::DEVELOPMENT, __ROOT__ . '/log');

/**
 * class loading
 */
$loader = new \Nette\Loaders\RobotLoader();
$loader->addDirectory(__ROOT__ . '/app');
$loader->addDirectory(__ROOT__ . '/src');
$loader->setTempDirectory(__TEMP_DIR__);
$loader->register();


$di_loader = new Nette\DI\ContainerLoader(__TEMP_DIR__);

$class = $di_loader->load(function ($compiler) {

    if (file_exists(__DIR__ . '/config/config.local.neon')) {
        $compiler->loadConfig(__DIR__ . '/config/config.local.neon');
    }

    $compiler->loadConfig(__DIR__ . '/config/common.neon');
});

