<?php

namespace Zoho\Services;

use GuzzleHttp\Client;
use Happsnow\Exceptions\RefreshTokenMissing;
use Happsnow\Exceptions\ZohoTokenRequestException;
use SimpleCache\SimpleCache;

/**
 * Class OAuthProvider
 * provides methods for comunications with ZOHO oauth2 endpoints for
 * - generating refresh_token
 * - obtain access_token
 *
 * @author Josef Šíma [josef.sima@altekpro.cz]
 * @package Zoho\Services
 */
class OAuthProvider
{
    const CACHE_REFRESH_TOKEN = 'refresh_token_data';
    const CACHE_ACCESS_TOKEN = 'access_token_data';

    /**
     * @var SimpleCache
     */
    private SimpleCache $cache;

    /**
     * @var null|string
     */
    private ?string $refreshToken;

    /**
     * @var null|string
     */
    private ?string $apiDomain;

    /**
     * @var string
     */
    private string $clientId;

    /**
     * @var string
     */
    private string $clientSecret;

    /**
     * @var string
     */
    private string $scope;

    /**
     * @var string
     */
    private string $currentUserEmail;

    /**
     * @var string
     */
    private string $tokenUrl;

    /**
     * OAuthProvider constructor.
     * @param SimpleCache $cache
     * @param array $config
     */
    public function __construct(SimpleCache $cache, array $config)
    {
        $this->cache = $cache;
        $this->clientId = $config['clientId'];
        $this->clientSecret = $config['clientSecret'];
        $this->scope = $config['scope'];
        $this->currentUserEmail = $config['currentUserEmail'];
        $this->tokenUrl = $config['tokenUrl'];
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAccessToken(): string
    {
        $access_token_data = $this->cache->get(self::CACHE_ACCESS_TOKEN, true);

        if (is_null($access_token_data)) {
            $access_token_data = $this->getAccessTokenFromServer();
            return $access_token_data['access_token'];
        } else {
            if ($this->checkTokenValidity((int) $access_token_data['valid_to'])) {
                return $access_token_data['access_token'];
            } else {
                $access_token_data = $this->getAccessTokenFromServer();
                return $access_token_data['access_token'];
            }
        }
    }

    /**
     * Method get refresh_token and access_token from ZOHO oAuth endpoint and store it into cache
     * @param $grantCode
     * @throws ZohoTokenRequestException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateToken($grantCode): void
    {
        $client = new Client();

        $response = $client->request(
            'POST',
            $this->getTokenUrl(),
            [
                    'form_params' => [
                        'grant_type'    => 'authorization_code',
                        'client_id'     => $this->getClientId(),
                        'client_secret' => $this->getClientSecret(),
                        'code'          => $grantCode
                    ]
                ]
        );

        if ($response->getStatusCode() == 200) {
            $responseData = json_decode($response->getBody()->getContents());

            if (isset($responseData->error)) {
                throw new ZohoTokenRequestException($responseData->error);
            }

            $this->cache->put(
                self::CACHE_REFRESH_TOKEN,
                [
                        'refresh_token' => $responseData->refresh_token,
                        'api_domain'    => $responseData->api_domain
                    ],
                true
            );

            $this->cache->put(
                self::CACHE_ACCESS_TOKEN,
                [
                'access_token' => $responseData->access_token,
                'valid_to'     => (time() + (int) $responseData->expires_in)
                    ],
                true
            );
        } else {
            throw new ZohoTokenRequestException('Some bad happens');
        }
    }

    /**
     * @return string
     */
    public function getTokenUrl(): string
    {
        return $this->tokenUrl;
    }

    /**
     * @return string
     * @throws RefreshTokenMissing
     */
    public function getRefreshToken(): string
    {
        if (!isset($this->refreshToken)) {
            $this->assetRefreshTokenDataFromCache();
        }

        return $this->refreshToken;
    }

    /**
     * @return string
     * @throws RefreshTokenMissing,
     */
    public function getApiDomain(): string
    {
        if (is_null($this->apiDomain)) {
            $this->assetRefreshTokenDataFromCache();
        }
        return $this->apiDomain;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->currentUserEmail;
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * @param int $valid_to
     * @return bool
     */
    private function checkTokenValidity(int $valid_to): bool
    {
        return time() < $valid_to;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getAccessTokenFromServer(): array
    {
        $client = new Client();

        $response = $client->request(
            'POST',
            $this->getTokenUrl(),
            [
                    'form_params' => [
                        'refresh_token' => $this->getRefreshToken(),
                        'client_id'     => $this->getClientId(),
                        'client_secret' => $this->getClientSecret(),
                        'grant_type'    => 'refresh_token',
                    ]
                ]
        );

        //TODO: STORE ACCESS TOKEN TO CACHE (persistent cache)

        if ($response->getStatusCode() == 200) {
            $responseData = json_decode($response->getBody()->getContents());

            if (isset($responseData->access_token)) {
                $accessTokenData = [
                    'access_token' => $responseData->access_token,
                    'valid_to'     => (time() + (int) $responseData->expires_in)
                ];

                $this->cache->put(self::CACHE_ACCESS_TOKEN, $accessTokenData, true);
                return $accessTokenData;
            }
        }

        throw new \Exception('something wrong when obtain access_token');
    }

    /**
     * @throws RefreshTokenMissing
     */
    private function assetRefreshTokenDataFromCache(): void
    {
        $refresh_token_data = $this->cache->get(self::CACHE_REFRESH_TOKEN, true);
        if (is_null($refresh_token_data)) {
            throw new RefreshTokenMissing();
        }

        $this->refreshToken = $refresh_token_data['refresh_token'];
        $this->apiDomain = $refresh_token_data['api_domain'];
    }
}
