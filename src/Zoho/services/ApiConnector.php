<?php

declare(strict_types=1);

namespace Zoho\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Nette\Utils\DateTime;

/**
 * @author hippo
 */
class ApiConnector
{

    /**
     * @var array
     */
    protected array $apiConfig;

    /**
     * @var \Zoho\Services\OAuthProvider
     */
    protected OAuthProvider $oauthProvider;

    /**
     * @var string
     */
    protected string $access_token;

    /**
     * @var string
     */
    protected string $baseUri;

    /**
     *
     * @var string
     */
    protected $timezone;

    /**
     *
     * @var DateTime;
     */
    protected $date;

    /**
     * ApiConnector constructor.
     * @param \Zoho\Services\OAuthProvider $oauthProvider
     * @param array $apiConfig
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __construct(OAuthProvider $oauthProvider, array $apiConfig)
    {
        $this->oauthProvider = $oauthProvider;
        $this->access_token = $this->oauthProvider->getAccessToken();
        $this->apiConfig = $apiConfig;
        $this->baseUri = $apiConfig['baseUri'];
        $this->timezone = new \DateTimeZone('America/North_Dakota/Center');
        $this->date = new DateTime('now', $this->timezone);
    }

    /**
     *
     * @param string $moduleName
     * @param string $searchName
     * @param string $search
     * @return string
     */
    public function searchZohoData(string $moduleName, string $searchName, string $search): string
    {
        $uri = $moduleName . '/search?criteria=((' . $searchName . ':equals:' . $search . '))';
        $response = $this->getGetResponse($uri);
        $body = json_decode($response->getBody()->getContents(), true);
        if (is_null($body) === false) {
            if (isset($body['data'][0])) {
                return $body['data'][0]['id'];
            }
        }
        return '';
    }

    /**
     *
     * @param string $moduleName
     * @return void
     */
    public function listZohoData(string $moduleName): void
    {
        $response = $this->getGetResponse($moduleName);
        echo '<pre>';
        die(print_r(json_decode($response->getBody()->getContents())));
    }

    /**
     *
     * @param string $module
     * @param array $fields
     * @param string $search
     * @return array
     */
    public function getDataModuleForSearch(string $module, array $fields, string $search): array
    {
        $data = [];
        $page = 0;
        do {
            $page++;
            $uri = $module . '?fields=' . join(',', $fields) . '&page=' . $page;
            $response = $this->getGetResponse($uri);
            $info = json_decode($response->getBody()->getContents(), true);
            if (empty($info) === false) {
                foreach ($info['data'] as $k => $val) {
                    $data[$val[$search]] = $val;
                }
            }
        } while ($this->getDataInfo($info['info']) === true);
        return $data;
    }

    /**
     *
     * @param string $uri
     * @return Response
     */
    private function getGetResponse(string $uri): Response
    {
        $client = new Client();
        $response = $client->request(
            'GET',
            $this->baseUri . $uri,
            [
                    'headers' => [
                        'Authorization' => 'Zoho-oauthtoken ' . $this->oauthProvider->getAccessToken()
                    ]
                ]
        );
        return $response;
    }

    /**
     *
     * @param string $uriParam
     * @param array $json
     * @return Response
     */
    private function getPostResponse(string $uriParam, array $json): Response
    {
        $client = new Client();
        $response = $client->request(
            'POST',
            $this->baseUri . $uriParam,
            [
                    'headers' => [
                        'Authorization' => 'Zoho-oauthtoken ' . $this->oauthProvider->getAccessToken()
                    ],
                    'json'    => $json
                ]
        );
        return $response;
    }

    /**
     *
     * @param array $data
     * @param string $moduleName
     * @return void
     */
    public function putZohoData(array $data, string $moduleName, string $method = '/upsert'): void
    {
        $response = $this->getPostResponse($moduleName . $method, $data);
        echo '<pre>';
        echo $response->getStatusCode() . "\n";
        echo "---------------------------------------\n";
        $return = \json_decode($response->getBody()->getContents(), false);
        if (empty($return->data) === false && is_object($return)) {
            foreach ($return->data as $key => $value) {
                dump($value);
            }
        }
        echo "---------------------------------------\n";
        //print_r(json_decode($response->getBody()->getContents()), false);
        echo 'END';
    }

    /**
     *
     * @return array
     */
    public function getTransactionForToday():array
    {
        $first = $this->date->modifyClone('-28 hours');
        $last = $this->date->modifyClone('+1 hour');
        $data = [];
        $page = (int) 0;
        $offset = (int) 200;
        $c = 1;
        do {
            $coql['select_query'] = "select Name from Transactions  where 
                Modified_Time between '" . $first->format('c') . "' and   
                '" . $last->format('c') . "' LIMIT " . $offset . " OFFSET " . $page;

            $page = $offset * $c;
            $c++;

            $response = $this->getPostResponse('coql', $coql);
            $info = \json_decode($response->getBody()->getContents(), true);
            if (empty($info) === false) {
                foreach ($info['data'] as $k => $val) {
                    $data[$val['Name']] = $val;
                }
            } else {
                $info['info']['more_records'] = false;
            }
        } while ($this->getDataInfo($info['info']) === true);
        return $data;
    }

    public function getTransactionsForEvent($event):array
    {
        $data = [];
        $page = (int) 0;
        $offset = (int) 200;
        $c = 1;
        do {
            $coql['select_query'] = "select Name from Transactions "
                    . "where Event = $event"
                    . " LIMIT " . $offset . " OFFSET " . $page;

            $page = $offset * $c;
            $c++;

            $response = $this->getPostResponse('coql', $coql);
            $info = \json_decode($response->getBody()->getContents(), true);
            if (empty($info) === false) {
                foreach ($info['data'] as $k => $val) {
                    $data[$val['Name']] = $val;
                }
            } else {
                $info['info']['more_records'] = false;
            }
        } while ($this->getDataInfo($info['info']) === true);
        return $data;
    }

    /**
     *
     * @return array
     */
    public function getEventsForPostEvents():array
    {
        $now = new DateTime('now', $this->timezone);
        $start = $now->modifyClone('-4 day');
        //$now->modify('-5 days');
        $data = [];
        $page = (int) 0;
        $offset = (int) 200;
        $c = 1;
        do {
            $coql['select_query'] = "select Event_Identification, Title, Name from Event  where 
                Event_Date between '".$start->format('Y-m-d')."' and '".$now->format('Y-m-d')."'"
                    . " LIMIT " . $offset . " OFFSET " . $page;
            /*$coql['select_query'] = "select Event_Identification, Title, Name from Event  where
                Event_Date ='" . $start->format('Y-m-d') . "' or Event_Date = '" . $now->format('Y-m-d') . "'"
                    . "LIMIT " . $offset . " OFFSET " . $page;*/
            //echo $coql['select_query'];
            //die();
            $page = $offset * $c;
            $c++;

            $response = $this->getPostResponse('coql', $coql);
            $info = \json_decode($response->getBody()->getContents(), true);

            if (empty($info) === false) {
                foreach ($info['data'] as $k => $val) {
                    $data[$val['Event_Identification']] = $val;
                }
            } else {
                $info['info']['more_records'] = false;
            }
        } while ($this->getDataInfo($info['info']) === true);
        return $data;
    }

    /**
     *
     * @return array
     */
    public function getCustomersForToday(): array
    {
        $this->date->modify('today');
        $last = $this->date->modifyClone('+1 day');
        $data = [];
        $page = (int) 0;
        $offset = (int) 200;
        $c = 1;
        do {
            $coql['select_query'] = "select Name,Customer_Name from Customers  where 
                Modified_Time >= '" . $this->date->format('c') . "' 
                AND Modified_Time < '" . $last->format('c') . "' LIMIT " . $offset . " OFFSET " . $page;

            $page = $offset * $c;
            $c++;

            $response = $this->getPostResponse('coql', $coql);
            $info = \json_decode($response->getBody()->getContents(), true);
            if (empty($info) === false) {
                foreach ($info['data'] as $k => $val) {
                    $data[$val['Name']] = $val;
                }
            } else {
                $info['info']['more_records'] = false;
            }
        } while ($this->getDataInfo($info['info']) === true);
        return $data;
    }

    /**
     *
     * @param array $data
     * @return bool
     */
    public function getDataInfo(array $data): bool
    {
        if ($data['more_records'] === true) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param string $id
     * @return string
     */
    public function getTransactionsForICS(string$id): ?string
    {
        $coql['select_query'] = "select Name from Transactions where Name='$id'";
        //dump($coql);
        $response = $this->getPostResponse('coql', $coql);
        if (!is_null($response)) {
            $info = \json_decode($response->getBody()->getContents(), true);
            //dump($info);
            if (empty($info['data']) === false) {
                return $info['data'][0]['id'];
            }
        }
        return null;
    }

    /**
     *
     * @param string $id
     * @return string
     */
    public function getEventsForICS(string $id): ?string
    {
        $coql['select_query'] = "select Name from Event where Event_Identification='$id'";
        //dump($coql);
        $response = $this->getPostResponse('coql', $coql);
        if (!is_null($response)) {
            $info = \json_decode($response->getBody()->getContents(), true);
            //dump($info);
            if (empty($info['data']) === false) {
                return $info['data'][0]['id'];
            }
        }
        return null;
    }

    /**
     *
     * @param string $id
     * @return string
     */
    public function getCustomersForICS(string $id): ?string
    {
        $coql['select_query'] = "select Name from Customers where Name='$id'";
        //dump($coql);
        $response = $this->getPostResponse('coql', $coql);
        if (!is_null($response)) {
            $info = \json_decode($response->getBody()->getContents(), true);
            //dump($info);
            if (empty($info['data']) === false) {
                return $info['data'][0]['id'];
            }
        }
        return null;
    }
}
