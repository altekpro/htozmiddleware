<?php

/*
 * Pharo
 */
declare(strict_types=1);

namespace Zoho\Inserts;

/**
 * @author hippo
 */
class ticket_types extends insert
{

    /**
     *
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param \Happsnow\Services\ApiConnector $apiConnector
     * @param string $module
     */
    public function __construct(
        \Zoho\Services\ApiConnector $targetContainer,
        \Happsnow\Services\ApiConnector $apiConnector
    )
    {
        $data = $apiConnector->getData('ticket_types', 'events', $targetContainer);
        //dump($data); die();
        $this->processInsert($data, $targetContainer, 'Ticket_Types');
    }
}
