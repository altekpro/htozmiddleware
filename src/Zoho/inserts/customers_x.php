<?php

/*
 * Pharo
 */
declare(strict_types=1);

namespace Zoho\Inserts;

/**
 * @author hippo
 */
class customers_x extends insert
{

    /**
     *
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param \Happsnow\Services\ApiConnector $apiConnector
     * @param string $module
     */
    public function __construct(
        \Zoho\Services\ApiConnector $targetContainer,
        \Happsnow\Services\ApiConnector $apiConnector,
        $module,
        $par
    ) {
        $t_params = $this->transactionParams();
        if ($par == 'final') {
            $t_params = $this->transactionParams('-72 hours');
        }
        $data = $apiConnector->getData(
            'customers_x',
            'transactions',
            $targetContainer,
            $t_params
        );
        $this->processInsert($data['events_x'], $targetContainer, 'Customers_X_Event');
        usleep(10);
        $this->processInsert($data['transactions_x'], $targetContainer, 'Customers_X_Transactions');
    }
}
