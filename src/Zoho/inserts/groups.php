<?php

/*
 * Pharo
 */
declare(strict_types=1);

namespace Zoho\Inserts;

/**
 * @author hippo
 */
class groups extends insert
{

    /**
     *
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param \Happsnow\Services\ApiConnector $apiConnector
     * @param string $module
     */
    public function __construct(
        \Zoho\Services\ApiConnector $targetContainer,
        \Happsnow\Services\ApiConnector $apiConnector,
        string $module
    ) {
        $data = $apiConnector
                ->getData(
                    $module,
                    $module,
                    $targetContainer
                );
        //dump( $data ); die();
        $this->processInsert($data, $targetContainer, 'Accounts', '');
    }
}
