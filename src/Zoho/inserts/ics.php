<?php

declare(strict_types=1);

namespace Zoho\Inserts;

/**
 * @author Tomáš Filip <dev@tomas-filip.com>
 * @since 1.0.0
 */
class ics extends insert
{
    /**
     *
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param \Happsnow\Services\ApiConnector $apiConnector
     * @param string $module
     */
    public function __construct(
        \Zoho\Services\ApiConnector $targetContainer,
        \Happsnow\Services\ApiConnector $apiConnector,
        string $module
    ) {
        $data = $apiConnector->getData('ics', 'events', $targetContainer);
        $this->processInsert($data, $targetContainer, 'ICS');
    }
}
