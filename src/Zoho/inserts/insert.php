<?php

/*
 * Pharo
 */
declare(strict_types=1);

namespace Zoho\Inserts;

use Nette\Utils\DateTime;
use DateTimeZone;

/**
 * @author hippo
 */
abstract class insert
{

    /**
     *
     * @param array $data
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param string $table
     * @return void
     */
    protected function processInsert(
        array $data,
        \Zoho\Services\ApiConnector $targetContainer,
        string $table,
        string $method = '/upsert'
    ): void {
        //dump($data);
        //die('INSERT');
        foreach ($data as $limit => $values) {
            $insert['data'] = $values;
            $insert['trigger'] = ["workflow"];
            $targetContainer->putZohoData($insert, $table, $method);
            usleep(1);
        }
    }

    /**
     *
     * @param string $init
     * @param string $last
     * @return string
     */
    protected function transactionParams(string $init = '-150 minutes'): string
    {
        $now = new DateTime('now', new DateTimeZone('America/North_Dakota/Center'));
        $start = $now->modifyClone($init);
        $end = new DateTime('NOW');
        return '?tx_start=' . $start->format('Y-m-d\TH:i') . '&tx_end=' . $end->format('Y-m-d\TH:i');
    }
}
