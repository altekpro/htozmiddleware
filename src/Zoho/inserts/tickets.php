<?php

/*
 * Pharo
 */
declare(strict_types=1);

namespace Zoho\Inserts;

/**
 * @author hippo
 */
class tickets extends insert
{

    /**
     *
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param \Happsnow\Services\ApiConnector $apiConnector
     */
    public function __construct(
        \Zoho\Services\ApiConnector $targetContainer,
        \Happsnow\Services\ApiConnector $apiConnector,
        $module,
        $par
    ) {
        $t_params = $this->transactionParams();
        if ($par == 'final') {
            $t_params = $this->transactionParams('-72 hours');
        }
        $data = $apiConnector->getData(
            'tickets',
            'transactions',
            $targetContainer,
            $t_params
        );
        $this->processInsert($data, $targetContainer, 'Tickets1');
    }
}
