<?php

namespace Happsnow\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Happsnow\Exceptions\UnauthorizedException;

/**
 * Class ApiLogin
 * provide login service for authorized access to happsnow API
 *
 * @author Josef Šíma [josef.sima@altekpro.cz]
 * @package Happsnow\Services
 */
class ApiLogin
{

    /**
     * @var string
     */
    private string $url;

    /**
     * @var string
     */
    private string $email;

    /**
     * @var string
     */
    private string $password;

    /**
     * ApiLogin constructor.
     * @param string $url
     * @param string $email
     * @param string $password
     */
    public function __construct(string $url, string $email, string $password)
    {
        $this->url = $url;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     * @throws UnauthorizedException
     * @throws GuzzleException
     */
    public function login(): string
    {
        $client = new Client();
        $response = $client->request(
            'POST',
            $this->url,
            [
                    'form_params' => [
                        'email'    => $this->email,
                        'password' => $this->password,
                    ]
                ]
        );

        $status = json_decode((string) $response->getBody(), true);
        if ($response->getStatusCode() == 200 && $status['status'] == "success") {
            //login success
            $cookieHeader = $response->getHeader('Set-Cookie');
            //get token from headers
            $matches = [];
            preg_match('/\=(.*)\;/', $cookieHeader[0], $matches);

            if (count($matches) == 2 && strlen($matches[1]) > 0) {
                return $matches[1];
            }
        }

        throw new UnauthorizedException();
    }
}
