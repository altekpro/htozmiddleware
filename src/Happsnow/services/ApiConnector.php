<?php

namespace Happsnow\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Happsnow\Exceptions\ModuleNotFound;
use Happsnow\Exceptions\UnauthorizedException;
use Happsnow\Utils\Transform;
use Nette\Utils\DateTime;
use DateTimeInterface;
use DateTimeZone;

/**
 * Class ApiConnector
 * provide access to happsnow api data
 *
 * @author Josef Šíma [josef.sima@altekpro.cz]
 * @author Tomáš Filip [tomas.filip@altekpro.cz]
 * @package Happsnow\Services
 */
class ApiConnector
{

    /**
     * date from to mine data
     * @todo move it to *.neon
     * @deprecated since version 1.5
     */
    const DATA_START = '2020-07-01';

    /**
     * Api insert limit
     * @todo move it to *.neon
     */
    const ZOHOAPI_LIMIT = 99;

    /**
     * @var ApiLogin
     */
    protected ApiLogin $apiLogin;

    /**
     * @var array
     */
    protected array $apiConfig;

    /**
     * @var array
     */
    protected array $modules;

    /**
     * @var array
     */
    protected array $endpoints;

    /**
     * @var string
     */
    protected string $token;

    /**
     * @var string
     */
    protected string $baseUri;

    /**
     * @var string
     */
    protected string $cookieDomain;

    /**
     * @var array
     */
    public array $data;

    /**
     * @var array
     */
    public array $inside;

    /**
     *
     * @var string
     */
    public $timezone;

    /**
     * ApiConnector constructor.
     * @param ApiLogin $loginService
     * @param array $apiConfig
     * @throws GuzzleException
     * @throws UnauthorizedException
     */
    public function __construct(ApiLogin $loginService, array $apiConfig)
    {
        $this->apiLogin = $loginService;
        /**
         * uncomment when go to produce. This is only for mock data
         */
        $this->token = $loginService->login();
        $this->endpoints = $apiConfig['modules'];
        $this->modules = array_keys($this->endpoints);
        $this->baseUri = $apiConfig['baseUri'];
        $this->cookieDomain = $apiConfig['cookieDomain'];
        $this->timezone = new DateTimeZone('America/North_Dakota/Center');
    }

    /**
     *
     * @param string $moduleName
     * @param string $apiName
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @param string $params
     * @return type
     */
    public function getData(
            string $moduleName, string $apiName, \Zoho\Services\ApiConnector $targetContainer, string $params = ''
    )
    {
        $this->validateModuleName($moduleName);

        //funcking workaround
        $rawData = [];
        if ($moduleName != 'post_event') {
            $rawData = $this->loadRawData($this->endpoints[$apiName], $params, 'normal');
        }
        $methodName = 'process' . ucfirst($moduleName);
        try {
            return $this->$methodName($rawData, $targetContainer);
        } catch (\Throwable $e) {
            \Tracy\Debugger::log($e, 'error');
        }
    }

    /**
     * @param string $endpoint
     * @param array $filterParams
     * @return array
     * @throws GuzzleException
     */
    private function loadRawData(
            string $endpoint, string $filterParams = '', string $flag = ''
    )
    : array
    {
        $url = sprintf('%s%s%s', $this->baseUri, $endpoint, $filterParams);
        //dump($url);
        $this->debugLog($url, $flag);

        $client = new Client([
            'base_uri' => $this->baseUri,
            'timeout'  => 240.0
        ]);
        try {
            $response = $client->request(
                    'GET', $endpoint . $filterParams,
                    [
                        'cookies' => CookieJar::fromArray(['hnsessid' => $this->token], $this->cookieDomain)
                    ]
            );
            if ($response->getStatusCode() == 200) {
                return \GuzzleHttp\json_decode($response->getBody(), true);
            }
        } catch (\Throwable $e) {
            \Tracy\Debugger::log($e, 'error');
        }
    }

    /**
     *
     * @param string $url
     * @param string $flag
     * @return void
     */
    private function debugLog(string $url, string $flag = ''): void
    {
        $now = new DateTime();
        $logfile = sprintf('%s/%s.%s', __ROOT__ . '/log', $now->format('d.m.Y'), "log");
        $handle = fopen($logfile, 'a');
        $txt = sprintf('%s :: %s#%s', $now->format('d.m.Y H:i:s'), $url, $flag . "\n");
        fwrite($handle, $txt);
        fclose($handle);
    }

    ##############################################
    ## Processes
    ##############################################

    /**
     *
     * @param array $rawData
     * @param string $moduleName
     * @param array $parent
     * @return array transformed data
     */
    private function processPartners(array $rawData, \Zoho\Services\ApiConnector $targetContainer): array
    {
        $a = 0;
        $c = 0;
        $this->data = [];
        foreach ($rawData['partners'] as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            if (isset($val['feeStructure']) === true) {
                unset($val['feeStructure']);
            }
            $this->data[$c][] = Transform::TransFlat(
                            Transform::PartnersMapA, Transform::PartnersMapB, $val
            );
            $a++;
        }  //end of full loop
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processEvent_managers(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $users = $this->sortUsersGetManagers($rawData['users']);
        $a = 0;
        $c = 0;
        $this->data = [];
        foreach ($users as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            $this->data[$c][] = Transform::TransFlat(
                            Transform::EventManagersMapA, Transform::EventManagersMapB, $val
            );
            $a++;
        }
        return $this->data;
    }

    /**
     *
     * @param array $array
     * @param string $search
     * @return string
     */
    private function processContact(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $a = 0;
        $c = 0;
        $this->data = [];
        $users = $this->sortContact($rawData['users']);
        $partners = $targetContainer->getDataModuleForSearch(
                'partners', ['Partner_ID', 'Name', 'id'], 'Partner_ID'
        );

        foreach ($users as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            $partner_id = $this->getParentByKey($partners, $val['organizationId']);
            if (empty($partner_id) === true) {
                continue;
            }
            $val['Partner_2'] = ['id' => $partner_id];
            $this->data[$c][] = Transform::TransFlat(
                            Transform::ContactMapA, Transform::ContactMapB, $val
            );
            $a++;
        }
        return $this->data;
    }

    private function sortContact(array $data): array
    {
        $sorted = [];
        foreach ($data as $key => $val) {
            if ((int) $val['type'] != 10 && $val['organizationId'] != "") {
                $sorted[] = $val;
            }
        }
        return $sorted;
    }

    /**
     *
     * @param array $data
     * @return array
     */
    private function sortUsersGetManagers(array $data): array
    {
        $sorted = [];
        foreach ($data as $key => $val) {
            if ((int) $val['type'] == 10) {
                $sorted[$val['id']] = $val;
            }
        }
        return $sorted;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processFee_structure(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $this->data = [];
        $parent = $targetContainer->getDataModuleForSearch(
                'partners', ['Partner_ID', 'Name', 'id'], 'Partner_ID'
        );
        foreach ($rawData['partners'] as $key => $val) {
            if (isset($val['feeStructure']) === true) {
                foreach ($val['feeStructure'] as $fk => $fv) {
                    $partner_id = $this->getParentByKey($parent, $val['id']);
                    if (empty($partner_id) === true) {
                        continue;
                    }
                    $fv['Partner'] = [
                        'id' => $partner_id
                    ];

                    $last = $fv['channel'] == 'any' ? 'any' : ucfirst($fv['channel']);
                    $fv['Free_Structure_Name'] = sprintf('%s-%s %s', $fv['beginning'], $fv['end'], $last);
                    $tmp[] = Transform::TransFlat(
                                    Transform::FeeStructureMapA, Transform::FeeStructureMapB, $fv,
                                    Transform::FeeStructureF
                    );
                }
            }
        }  //end of full loop
        $a = 0;
        $c = 0;
        foreach ($tmp as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            $this->data[$c][] = $val;
            $a++;
        }
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param string $moduleName
     * @param array $parents
     * @return array
     */
    private function processGroups(array $rawData, \Zoho\Services\ApiConnector $targetContainer): array
    {
        $this->data = [];
        $a = 0;
        $c = 0;
        $parents = $targetContainer
                ->getDataModuleForSearch(
                'partners', ['Partner_ID', 'Name', 'id'], 'Partner_ID'
        );
        foreach ($rawData['groups'] as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            if (isset($val['promoCodes']) === true) {
                unset($val['promoCodes']);
            }
            $partner_id = $this->getParentByKey($parents, $val['ownerId']);
            if (empty($partner_id)) {
                continue;
            }
            $val['Partner'] = [
                'id' => $partner_id
            ];
            $val['Group_Happs_ID'] = $val['id'];
            //@TODO refactor it to the array_slice
            unset($val['accessTypes']);
            unset($val['slug']);
            unset($val['headerMedia']);
            unset($val['showcaseMedia']);
            unset($val['showcaseMediaPoster']);
            unset($val['id']);
            $this->data[$c][] = Transform::TransFlat(
                            Transform::GroupsMapA, Transform::GroupsMapB, $val
            );

            $a++;
        }//end of full loop
        return $this->data;
    }

    /**
     *
     * @param string $init
     * @param string $last
     * @return string
     */
    protected function transactionParams(string $init = '-130 minutes')
    {
        $now = new DateTime('now', new DateTimeZone('America/North_Dakota/Center'));
        $start = $now->modifyClone($init);
        return '?tx_start=' . $start->format('Y-m-d\TH:i') . '&tx_end=' . $now->format('Y-m-d\TH:i');
    }

    protected function processPost_event(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $this->data = [];

        $events = $targetContainer->getEventsForPostEvents();
        if (empty($events)) {
            die('No-DATA');
        }
        //dump($rawData);\
        //dump($events);
        //die();
        $t_id = [];
        foreach ($events as $key => $event) {
            $transactions = $targetContainer->getTransactionsForEvent($event['id']);
            $param = '?eid=' . $key;
            $transaction_data = $this->loadRawData('transactions/export', $param, 'post_events');
            //dump($transactions);
            // dump($transaction_data);die();
            foreach ($transaction_data as $k => $val) {
                $transactions_id = $this->getParentByKey($transactions, $val['transaction_id']);
                if (empty($transactions_id) === true) {
                    $t_id[$val['transaction_id']] = 1;
                    continue;
                }
                if (empty($val['ticket_last_scanned']) or $val['ticket_last_scanned'] == 'null') {
                    $val['ticket_last_scanned'] = null;
                } else {
                    $last = new DateTime($val['ticket_last_scanned']);
                    $val['ticket_last_scanned'] = $last->format('c');
                }
                $val['Transaction'] = ['id' => $transactions_id];
                $val['Event'] = ['id' => $event['id']];
                $val['Name'] = $val['ticket_id'] . ' ' . $val['ticket_type'];
                unset($val['ticket_type']);

                foreach ($val as $k => $vu) {
                    if (preg_match('/transaction_/', $k)) {
                        unset($val[$k]);
                    }
                    if (preg_match('/customer_/', $k)) {
                        unset($val[$k]);
                    }
                    if (preg_match('/event_/', $k)) {
                        //unset($val[$k]);
                    }
                    if (preg_match('/partner_/', $k)) {
                        unset($val[$k]);
                    }
                    if (preg_match('/group_/', $k)) {
                        unset($val[$k]);
                    }
                }
                $full_tmp[] = Transform::TransFlat( 
                        Transform::TicketsMapA, 
                        Transform::TicketsMapB, 
                        $val, 
                        Transform::TicketsF
                );
            }
        }
        $a = 0;
        $c = 0;
        //dump($full_tmp);die();
        foreach ($full_tmp as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            $this->data[$c][] = $val;
            $a++;
        }
        if (!empty($t_id)) {
            echo \GuzzleHttp\json_encode($t_id);
        }
        if (empty($this->data)) {
            die('NO-TRANSFORMED DATA');
        }
        //dump($this->data);
        //die();
        return $this->data;
    }

    protected function processPost_event_old(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $this->data = [];
        $events = [];
        $now = new DateTime('now', $this->timezone);
        /** doit with modify */
        $now = new DateTime($now->format('Y-m-d H') . ':00:00', $this->timezone);
        $now->modify('-24 hours');
        $next = $now->modifyClone('+25 hour');
        $date_tmp = [];

        foreach ($rawData['events'] as $key => $val) {
            if (empty($val['date']) || empty($val['timeZone'])) {
                continue;
            }
            $d = new DateTime($val['date']);
            $timezone = new DateTimeZone($val['timeZone']);
            $date = new DateTime($d->format('Y-m-d') . ' ' . $val['startTime'], $timezone);
            if ($date->getTimestamp() >= $now->getTimestamp() && $date->getTimestamp() < $next->getTimestamp()) {
                $events[$val['eventId']] = $val;
                $date_tmp[$date->getTimestamp()] = $date;
            }
        }
        if (empty($events)) {
            die('No-DATA');
        }
        $events_in_zoho = $targetContainer->getEventsForPostEvents();
        $transactions = $targetContainer->getTransactionForToday();
        $low_date = array_key_first($date_tmp);
        $lowest = $date_tmp[$low_date];
        /* dump($events_in_zoho);
          dump($events);
          dump($transactions); */

        $now = new DateTime('now');

        $param = '?tx_start=' . $lowest->format('Y-m-d\TH:i') . '&tx_end=' . $now->format('Y-m-d\TH:i');
        $transaction_data = $this
                ->loadRawData('transactions/export', $param, 'post_events');
        //dump($transaction_data);

        $tmp = [];
        $full_tmp = [];
        $t_id = [];
        foreach ($events as $k => $v) {
            //dump($v);
            foreach ($transaction_data as $key => $val) {
                if ($val['event_id'] == $v['id']) {
                    foreach ($val as $in => $value) {
                        if (preg_match('/ticket/', $in) === false) {
                            continue;
                        }
                    }
                    $transactions_id = $this->getParentByKey($transactions, $val['transaction_id']);
                    if (empty($transactions_id) === true) {
                        $t_id[$val['transaction_id']] = 1;
                        continue;
                    }
                    $val['Transaction'] = [
                        'id' => $transactions_id
                    ];

                    $event_id = $this->getParentByKey($events_in_zoho, $val['event_id']);
                    if (empty($event_id) === true) {
                        echo 'event-' . $val['event_id'] . '<br>';
                        continue;
                    }
                    $val['Event'] = [
                        'id' => $event_id
                    ];
                    if (empty($val['ticket_last_scanned'])) {
                        $val['ticket_last_scanned'] = null;
                    } else {
                        $last = new DateTime($val['ticket_last_scanned']);
                        $val['ticket_last_scanned'] = $last->format('c');
                    }

                    $val['Name'] = $val['ticket_id'] . ' ' . $val['ticket_type'];
                    unset($val['ticket_type']);
                    foreach ($val as $k => $vu) {
                        if (preg_match('/transaction_/', $k)) {
                            unset($val[$k]);
                        }
                        if (preg_match('/customer_/', $k)) {
                            unset($val[$k]);
                        }
                        if (preg_match('/event_/', $k)) {
                            //unset($val[$k]);
                        }
                        if (preg_match('/partner_/', $k)) {
                            unset($val[$k]);
                        }
                        if (preg_match('/group_/', $k)) {
                            unset($val[$k]);
                        }
                    }
                    $full_tmp[] = Transform::TransFlat(
                                    Transform::TicketsMapA, Transform::TicketsMapB, $val, Transform::TicketsF
                    );
                }
            }
        }
        $a = 0;
        $c = 0;
        foreach ($full_tmp as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            $this->data[$c][] = $val;
            $a++;
        }
        if (!empty($t_id)) {
            echo \GuzzleHttp\json_encode($t_id);
        }
        if (empty($this->data)) {
            die('NO-TRANSFORMED DATA');
        }
        //dump($this->data);
        //die();
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param string $moduleName
     * @param array $parents
     * @return array
     */
    private function processEvents(array $rawData, \Zoho\Services\ApiConnector $targetContainer): array
    {
        //dump($rawData);
        //die();
        $this->data = [];

        $managers = $targetContainer->getDataModuleForSearch(
                'event_managers', ['Event_Manager_ID', 'Email', 'id'], 'Event_Manager_ID'
        );

        $parents = $targetContainer->getDataModuleForSearch(
                'accounts', ['Group_Happs_ID', 'Account_Name', 'id'], 'Group_Happs_ID'
        );
        $a = 0;
        $c = 0;
        foreach ($rawData['events'] as $key => $val) {
            $d = new DateTime($val['date']);
            $e = new DateTime($val['date']);
            $e->modify('-1 month');
            if ($d->getTimestamp() < $e->getTimestamp()) {
                continue;
            }

            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            unset($val['slug']);
            unset($val['headerMedia']);
            $val['promoCode'] = '';
            if (isset($val['icsSettings'])) {
                if (isset($val['icsSettings']['promoCode'])) {
                    $val['promoCode'] = $val['icsSettings']['promoCode'];
                }
                if (is_array($val['icsSettings']) && isset($val['icsSettings']['enabled'])) {
                    $val['icsSettings'] = $val['icsSettings']['enabled'];
                } else {
                    $val['icsSettings'] = null;
                }
            }



            if (isset($val['ticketTypes'])) {
                unset($val['ticketTypes']);
            }

            if ($val['managerId'] != null) {
                $manager_id = $this->getParentByKey($managers, $val['managerId']);
                if (empty($manager_id) === false) {
                    $val['Event_Manager'] = ['id' => $manager_id];
                }
            }

            $group_id = $this->getParentByKey($parents, $val['parentGroupId']);
            if (empty($group_id)) {
                continue;
            }
            $val['Parent_Group'] = ['id' => $group_id];

            $d = new DateTime($val['date']);
            $e = new DateTime($val['date']);
            /*
              $start_time = new DateTime(self::DATA_START);

              if ($start_time->getTimestamp() > $d->getTimestamp()) {
              continue;
              }
             */
            if (isset($val['isSeries'])) {
                if ($val['isSeries'] == false) {
                    $val['Is_Series'] = 'false';
                    //continue;
                } else {
                    $val['Is_Series'] = 'true';
                }
            }

            if (isset($val['seriesMembers'])) {
                if (is_array($val['seriesMembers'])) {
                    $val['Series_Members'] = join(",\n", $val['seriesMembers']);
                }
            }
            $date = new DateTime($d->format('Y-m-d') . ' ' . $val['startTime']);
            $val['description'] = $val['title'] . ' ' . $date->format(DateTimeInterface::RSS);
            //$val['Event_Date_ISO'] = $date->format('c');
            $val['Capacity_Sold'] = $val['capacitySold'];
            //$val['Submited'] = 'Submitted';
            $this->data[$c][] = Transform::TransFlat(
                            Transform::EventsMapA, Transform::EventsMapB, $val, Transform::EventsF
            );
            $a++;
        }  //end of full loop
        //dump($this->data);
        //die('EVENTS');
        return $this->data;
    }

    /**
     *
     * @param array $data
     * @return array
     */
    private function sortTransactions(array $data): array
    {
        return $data;
        $rawData = [];
        $today = new DateTime('now', $this->timezone);
        $today->modify('yesterday +1 day');
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $transDate = new DateTime($v['transaction_date']);
                if ($today->format('Ymd') != $transDate->format('Ymd')) {
                    continue;
                }
                $rawData[] = $v;
            }
        } else {
            return [];
        }
        return $rawData;
    }

    /**
     *
     * @param array $data
     * @return array
     */
    private function sortCustomers(array $data): array
    {
        $rawData = [];
        foreach ($data as $k => $v) {
            $rawData[$v['customer_email']] = $v;
        }
        return $rawData;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processTransactions(array $rawData, \Zoho\Services\ApiConnector $targetContainer): array
    {
        //$rawData = $this->sortTransactions($rawData);
        //dump( $rawData);
        $_raw = [];
        foreach ($rawData as $k => $v) {
            $_raw[$v['transaction_id']] = $v;
        }
        $rawData = $_raw;
        //dump( $rawData); die();
        $this->data = [];
        $partners = [];
        $events = [];
        $groups = [];

        $partners = $targetContainer->getDataModuleForSearch(
                'partners', ['Partner_ID', 'Name', 'id'], 'Partner_ID'
        );
        $events = $targetContainer->getDataModuleForSearch(
                'event', ['Event_Identification', 'Title', 'Name', 'id'], 'Event_Identification'
        );
        $groups = $targetContainer->getDataModuleForSearch(
                'accounts', ['Group_Happs_ID', 'Acount_Name', 'id'], 'Group_Happs_ID'
        );

        $a = 0;
        $c = 0;
        foreach ($rawData as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            //if ( $val['transaction_id'] != 10130 ) {
            //    continue;
            //}

            foreach ($val as $k => $v) {
                if (preg_match('/ticket_/', $k)) {
                    unset($val[$k]);
                }
            }

            $partner_id = $this->getParentByKey($partners, $val['partner_id']);
            if (empty($partner_id) === true) {
                continue;
            }
            $event_id = $this->getParentByKey($events, $val['event_id']);
            if (empty($event_id) === true) {
                continue;
            }
            $group_id = $this->getParentByKey($groups, $val['group_id']);
            if (empty($group_id) === true) {
                continue;
            }

            $val['Partner_Name'] = ['id' => $partner_id];
            //eventy
            $val['Event'] = ['id' => $event_id];

            $val['Group'] = ['id' => $group_id];

            $date = new DateTime($val['event_date']);
            $e_iso = new DateTime($val['event_date_iso']);
            $val['Event_Date_ISO'] = $e_iso->format('c');
            if (!isset($val['transaction_purchase_code'])) {
                $val['transaction_purchase_code'] = null;
            }

            /**
             * convert date
             */
            $e_iso_date = new DateTime($val['event_date_iso']);
            $Z = $e_iso_date->format('Z');
            $Z = $Z / 3600;
            $t_iso_date = new DateTime($val['transaction_date_iso']);
            $new_iso = $t_iso_date->modifyClone($Z . ' hours');
            $val['Transaction_Date_ISO'] = $new_iso->format('c');
            $val['Event_Time'] = $date->format('H:i:s');
            $val['Amount'] = $val['transaction_price']; //$val['customer_email'];
            $val['Description'] = 'Tickets for ' .
                    $val['customer_name'] . ' ' .
                    $val['customer_email'] . ' ' .
                    $val['event_title'] . ' ' .
                    $val['event_date'] . ' ';
            $this->data[$c][] = Transform::TransFlat(
                            Transform::TransactionsMapA, Transform::TransactionsMapB, $val, Transform::TransactionsF
            );
            $a++;
        }//end of full loop
        return $this->data;
    }

    private function convertISOTime($event_date_iso, $transaction_date)
    {
        
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processPromo_codes(array $rawData, \Zoho\Services\ApiConnector $targetContainer): array
    {
        //_print_d($parents); die();
        $groups = $targetContainer
                ->getDataModuleForSearch(
                'accounts', [
                    'Group_Happs_ID', 'Account_Name', 'id'
                ], 'Group_Happs_ID'
        );
        $this->data = [];
        $tmp = [];
        foreach ($rawData['groups'] as $key => $val) {
            if (empty($val['promoCodes']) === false) {
                foreach ($val['promoCodes'] as $fk => $fv) {
                    //$fv['test'] = $val['id'];
                    $group_id = $this->getParentByKey($groups, $val['id']);
                    if (empty($group_id) === true) {
                        continue;
                    }
                    $fv['Group'] = [
                        'id' => $group_id
                    ];
                    $fv['Name'] = $val['id'] . '-' . $fv['code'];
                    $tmp[] = Transform::TransFlat(
                                    Transform::PromoCodesMapA, Transform::PromoCodesMapB, $fv
                    );
                }
            }
        }
        $a = 0;
        $c = 0;
        foreach ($tmp as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            $this->data[$c][] = $val;
            $a++;
        }
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processTicket_Types(array $rawData, \Zoho\Services\ApiConnector $targetContainer): array
    {
        $this->data = [];
        $tmp = [];
        //dump($rawData); die();
        $events = $targetContainer->getDataModuleForSearch(
                'event', ['Event_Identification', 'Title', 'Name', 'id'], 'Name'
        );
        foreach ($rawData['events'] as $i => $event) {
            $in = [];
            $d = new DateTime($event['date']);
            $e = new DateTime($event['date']);
            $e->modify('-1 month');
            if ($d->getTimestamp() < $e->getTimestamp()) {
                continue;
            }
            $event_id = $this->getParentByKey($events, $event['eventId']);
            if (empty($event_id) === true) {
                continue;
            }
            $in['Event'] = ['id' => $event_id];
            $in['Event_name_test'] = $event['title'];
            $in['Event_Name'] = $this->getEventTitle($events, $event['eventId']);
            if (isset($event['ticketTypes'][0]['category'])) {
                foreach ($event['ticketTypes'] as $k => $values) {
                    unset($values['category']);
                    foreach ($values as $ki => $va) {
                        foreach ($va as $ka => $vi) {
                            $in['Ticket_Description'] = $vi['label'];
                            $in['Ticket_Type_ID'] = $vi['ticketType'];
                            $in['Ticket_Price'] = $vi['price'];
                            $in['Name'] = $vi['id'];
                            $in['JEDNA'] = 1;
                            $tmp[] = $in;
                        }
                    }
                }
            } else {
                foreach ($event['ticketTypes'] as $k => $vi) {
                    $in['Ticket_Description'] = $vi['ticketLabel'];
                    $in['Ticket_Type_ID'] = $vi['ticketType'];
                    $in['Ticket_Price'] = $vi['ticketPrice'];
                    $in['Name'] = $vi['id'];
                    $in['DVE'] = 1;
                    $tmp[] = $in;
                }
            }
        }
        $a = 0;
        $c = 0;
        foreach ($tmp as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }
            //$this->data[] = $val;
            $this->data[$c][] = $val;
            $a++;
        }
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param array $parents
     * @return array
     */
    private function processTickets(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        //$rawData = $this->sortTransactions($rawData);
        //dump($rawData);die();
        $events = $targetContainer->getDataModuleForSearch(
                'event', ['Event_Identification', 'Title', 'Name', 'id'], 'Event_Identification'
        );
        $transactions = $targetContainer->getTransactionForToday();
        $this->data = [];
        $a = 0;
        $c = 0;
        //dump($rawData);die();
        //_print_d ( $rawData);
        foreach ($rawData as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }

            $transactions_id = $this->getParentByKey($transactions, $val['transaction_id']);
            if (empty($transactions_id) === true) {
                continue;
            }
            $val['Transaction'] = [
                'id' => $transactions_id
            ];

            $event_id = $this->getParentByKey($events, $val['event_id']);
            if (empty($event_id) === true) {
                continue;
            }
            $val['Event'] = [
                'id' => $event_id
            ];

            if (!empty($val['ticket_last_scanned'])) {
                $last_scanned = new DateTime($val['ticket_last_scanned']);
                $val['ticket_last_scanned'] = $last_scanned->format('c');
            }

            $val['Name'] = $val['ticket_id'] . ' ' . $val['ticket_type'];
            unset($val['ticket_type']);
            foreach ($val as $k => $v) {
                if (preg_match('/transaction_/', $k)) {
                    unset($val[$k]);
                }
                if (preg_match('/customer_/', $k)) {
                    unset($val[$k]);
                }
                if (preg_match('/event_/', $k)) {
                    //unset($val[$k]);
                }
                if (preg_match('/partner_/', $k)) {
                    unset($val[$k]);
                }
                if (preg_match('/group_/', $k)) {
                    unset($val[$k]);
                }
            }

            $this->data[$c][] = Transform::TransFlat(
                            Transform::TicketsMapA, Transform::TicketsMapB, $val, Transform::TicketsF
            );
            $a++;
        }//end of full loop
        //dump($this->data);
        //die('TICKETS');
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processCustomers(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $rawData = $this->sortTransactions($rawData);
        $rawData = $this->sortCustomers($rawData);
        //dump($rawData);
        //die();

        $events = $targetContainer->getDataModuleForSearch(
                'event', ['Event_Identification', 'Title', 'Name', 'id'], 'Event_Identification'
        );
        $transactions = $targetContainer->getTransactionForToday();
        $this->data = [];
        $tmp = [];
        $sort = [];
        $a = 0;
        $c = 0;
        foreach ($rawData as $key => $val) {
            $sort[$val['customer_email']] = $val;
        }
        foreach ($sort as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }

            $transactions_id = $this->getParentByKey($transactions, $val['transaction_id']);
            if (empty($transactions_id) === true) {
                continue;
            }
            $val['Transaction'] = [
                'id' => $transactions_id
            ];

            $event_id = $this->getParentByKey($events, $val['event_id']);
            if (empty($event_id) === true) {
                continue;
            }
            $val['Event'] = [
                'id' => $event_id
            ];
            $tmp['Customer_Name'] = $val['customer_name'];
            $tmp['Name'] = $val['customer_email'];
            $tmp['Customer_Phone'] = $val['customer_phone'];
            $this->data[$c][] = $tmp;
            $a++;
        }//end of full loop
        return $this->data;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    private function processCustomers_x(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {
        $data = $this->sortTransactions($rawData);
        $rawData = $this->sortCustomers($data);

        $this->data = [];
        $this->inside = [];
        $events = $targetContainer->getDataModuleForSearch(
                'event', ['Event_Identification', 'Title', 'Name', 'id'], 'Event_Identification'
        );
        $transactions = $targetContainer->getTransactionForToday();
        $customers = $targetContainer->getCustomersForToday();
        $a = 0;
        $c = 0;
        //die();
        foreach ($rawData as $key => $val) {
            if ($a > self::ZOHOAPI_LIMIT) {
                $c++;
                $a = 0;
            }



            $event_id = $this->getParentByKey($events, $val['event_id']);
            if (empty($event_id) === true) {
                continue;
            }

            $transactions_id = $this->getParentByKey($transactions, $val['transaction_id']);
            if (empty($transactions_id) === true) {
                continue;
            }

            $customer_id = $this->getParentByKey($customers, $val['customer_email']);
            if (empty($customer_id) === true) {
                continue;
            }
            $x_events = [
                'Customers' => [
                    'id' => $customer_id
                ],
                'Event_s'   => [
                    'id' => $event_id
                ],
            ];
            $x_transactions = [
                'Customer_s'    => [
                    'id' => $customer_id
                ],
                'Transaction_s' => [
                    'id' => $transactions_id
                ]
            ];
            $this->data[$c][] = $x_events;
            $this->inside[$c][] = $x_transactions;
            $a++;
        }

        $output = ['events_x' => $this->data, 'transactions_x' => $this->inside];
        return $output;
    }

    /**
     *
     * @param array $rawData
     * @param \Zoho\Services\ApiConnector $targetContainer
     * @return array
     */
    public function processIcs(
            array $rawData, \Zoho\Services\ApiConnector $targetContainer
    ): array
    {

        //dump($rawData);
        $indata = [];
        $this->data = [];
        $a = 0;
        $c = 0;
        foreach ($rawData['events'] as $key => $val) {
            if (isset($val['icsSettings']['enabled']) && $val['icsSettings']['enabled'] == true) {
                $eid = $val['id'];
                $pch = $this->loadRawData('purchase_codes/export', '?eid=' . $eid, 'ics');
                if (!empty($pch)) {
                    $indata[$eid] = $pch;
                }
            }
        }

        foreach ($indata as $k => $v) {
            foreach ($v as $key => $val) {
                if ($a > self::ZOHOAPI_LIMIT) {
                    $c++;
                    $a = 0;
                }
                $tid = $targetContainer->getTransactionsForICS((string) $val['transactionId']);
                if (empty($tid)) {
                    continue;
                }
                $eid = $targetContainer->getEventsForICS((string) $val['eventId']);
                if (empty($eid)) {
                    continue;
                }
                $cid = $targetContainer->getCustomersForICS((string) $val['ownerEmail']);
                if (empty($cid)) {
                    continue;
                }
                $icss_in['Name'] = $val['code'];
                $icss_in['Owner_Member_ID'] = $val['ownerMemberId'];
                $icss_in['Purchases'] = $val['purchases'];
                $icss_in['Transaction_ID'] = ['id' => $tid];
                $icss_in['Event_ID'] = ['id' => $eid];
                $icss_in['Owner_Email'] = ['id' => $cid];
                $this->data[$c][] = $icss_in;
            }
        }
        return $this->data;
    }

    /**
     * @param $moduleName
     * @throws ModuleNotFound
     */
    private function validateModuleName($moduleName): void
    {
        if (!in_array($moduleName, $this->modules)) {
            throw new ModuleNotFound($moduleName);
        }
    }

    /**
     *
     * @param array $array
     * @param string $search
     * @return string
     */
    private function getEventTitle(array $array, string $search): string
    {
        if (array_key_exists($search, $array)) {
            return $array[$search]['Title'];
        }
    }

    /**
     *
     * @param array $array
     * @param string $search
     * @return string
     */
    private function getArrayField(array $array, string $search, string $field): string
    {
        if (array_key_exists($search, $array)) {
            return $array[$search][$field];
        }
        return '';
    }

    /**
     *
     * @param array $array
     * @param string $search
     * @return string
     */
    private function getParentByKey(array $array, string $search): string
    {
        if (array_key_exists($search, $array)) {
            return $array[$search]['id'];
        }
        return '';
    }

    /**
     *
     * @param array $array
     * @param string $search
     * @return string
     */
    private function getParentNameByKey(array $array, string $search, string $name = 'Name'): string
    {
        if (array_key_exists($search, $array)) {
            return $array[$search][$name];
        }
        return '';
    }

}
