<?php

/*
 * Pharo
 */
declare(strict_types=1);

namespace Happsnow\Utils;

use Nette\Utils\DateTime;

/**
 * @author hippo
 */
class Transform
{

    /**
     * @var array
     */
    const PartnersMapA = [
        '/currency/',
        '/gaId/',
        '/email/',
        '/name/',
        '/phone/',
        '/id/',
        '/paymentProcessor/',
    ];

    /**
     * @var array
     */
    const PartnersMapB = [
        'Currency',
        'GA_ID',
        'Email_1',
        'Name',
        'Phone_Number',
        'Partner_ID',
        'Stripe_Status',
    ];

    /**
     * @var array
     */
    const PartnersF = [];

    /**
     * @var array
     */
    const FeeStructureMapA = [
        '/id/',
        '/beginning/',
        '/end/',
        '/fee/',
        '/ceiling/',
        '/type/',
        '/rebate/',
        '/channel/',
    ];

    /**
     * @var array
     */
    const FeeStructureMapB = [
        'Name',
        'Beginning_2',
        'Ending_2',
        'Value_2',
        'Ceiling3',
        'Type',
        'Rebate',
        'Channel',
    ];

    /**
     * @var array
     */
    const FeeStructureF = [
        'rebate' => 'yesno',
    ];

    /**
     * @var array
     */
    const GroupsMapA = [
        '/title/',
        '/description/',
        '/category/',
    ];

    /**
     * @var array
     */
    const GroupsMapB = [
        'Account_Name',
        'Description',
        'Group_Category',
    ];

    /**
     * @var array
     */
    const PromoCodesMapA = [
        '/type/',
        '/code/',
        '/value/',
    ];

    /**
     * @var array
     */
    const PromoCodesMapB = [
        'Promo_Type',
        'Promo_Code',
        'Promo_Value'
    ];

    /**
     * @var array
     */
    const EventsMapA = [
        '/date/',
        '/title/',
        '/eventId/',
        '/fbId/',
        '/venueName/',
        '/description/',
        '/startTime/',
        '/id/',
        '/timeZone/',
        '/capacity/'
    ];

    /**
     * @var array
     */
    const EventsMapB = [
        'Event_Date',
        'Title',
        'Name',
        'Facebook_ID',
        'Event_Location',
        'Event_Description',
        'Event_Time',
        'Event_Identification',
        'Timezone',
        'Venue_Capacity'
    ];

    /**
     * @var array
     */
    const EventsF = [
        'date' => 'doendate'
    ];

    /**
     * @var array
     */
    const TicketTypesMapA = [
        '/ticketLabel/',
        '/ticketPrice/',
        '/ticketType/',
        '/id/',
        '/ticked_transfered/'
    ];

    /**
     * @var array
     */
    const TicketTypesMapB = [
        'Ticket_Description',
        'Ticket_Price',
        'Name',
        'Ticket_Type_ID',
        'Ticket_Transferred'

    ];

    /**
     * @var array
     */
    const TransactionsMapA = [
        '/transaction_id/',
        '/transaction_price/',
        '/transaction_conv_fees/',
        '/customer_email/',
        '/customer_name/',
        '/event_date/',
        '/event_title/',
        '/transaction_proc_fees/',
        '/transaction_promo/',
        '/transaction_rebates/',
        '/transaction_date/',
        '/transaction_type/',
        '/payment_receipt/',
        '/payment_processor/',
        '/payment_id/',
        '/transfer_id/',
        '/transaction_group_id/',
        '/payment_method/',
        '/payment_channel/',
        '/customer_postal/',
        '/customer_country/',
        '/customer_city/',
        '/customer_state_province/',
        '/transaction_purchase_code/',
    ];

    /**
     * @var array
     */
    const TransactionsMapB = [
        'Name',
        'AmountEdit',
        'Conv_Fee',
        'Customer_Email_2',
        'Customer_Name',
        'Event_Date',
        'Event_Name',
        'Proc_Fee',
        'Promo_Code',
        'Rebate_Fee',
        'Transaction_Date',
        'Transaction_Type',
        'Payment_Receipt',
        'Payment_Processor',
        'Payment_ID',
        'Transfer_ID',
        'Transaction_Group_ID1',
        'Payment_Method',
        'Payment_Channel',
        'Customer_Zip_Code',
        'Customer_Country',
        'Customer_City',
        'Customer_State',
        'Transaction_Purchase_Code'
    ];

    /**
     * @var array
     */
    const TransactionsF = [
        'event_date'       => 'doendate',
        'transaction_date' => 'doiso8601date',
    ];

    /**
     * @var array
     */
    const TicketsMapA = [
        '/ticket_id/',
        //'/ticket_type/',
        '/ticket_type_id/',
        //'/ticket_type_id_internal/',
        '/ticket_price/',
        '/ticket_promo_value/',
        '/ticket_conv_fees/',
        '/ticket_rebates/',
        '/ticket_proc_fees/',
        '/ticket_count/',
        '/payment_method/',
        '/ticket_invalidated/',
        '/ticket_scans/',
        '/ticket_url/',
        '/ticket_transferred/',
        '/ticket_last_scanned/'
    ];

    /**
     * @var array
     */
    const TicketsMapB = [
        'Ticket_ID',
        'Ticket_Name',
        //'Ticket_Type',
        //'Associated_Fee_Structure',
        'Ticket_Price',
        'Promo_Total',
        'Conv_Fees',
        'Rebates',
        'Proc_Fees',
        'Ticket_Count',
        'Comp',
        'Ticket_Invalidated',
        'Ticket_Scans',
        'Receipt',
        'Ticket_Transferred',
        'ticket_last_scanned'

    ];

    /**
     * @var array
     */
    const TicketsF = [
        'payment_method'     => 'comp_special',
        'ticket_invalidated' => 'true_false',
        'ticket_transferred' => 'true_false'
    ];

    /**
     * @var array
     */
    const CustomersMapA = [
        '/customer_email/',
        '/customer_name/',
        '/customer_phone/',
    ];

    /**
     * @var array
     */
    const CustomersMapB = [
        'Name',
        'Customer_Name',
        'Customer_Phone',
    ];

    /**
     * @var array
     */
    const EventManagersMapA = [
        '/id/',
        '/name/',
        '/email/'
    ];

    /**
     * @var array
     */
    const EventManagersMapB = [
        'Event_Manager_ID',
        'Name',
        'Email'
    ];


    const ContactMapA = [
        '/id/',
        '/email/',
        '/type/'
    ];

    const ContactMapB = [
        'HappsNow_ID',
        'Email',
        'Type_ID'
    ];


    /**
     *
     * @param array $A
     * @param array $B
     * @param array $data
     * @param array $F
     * @return array
     */
    public static function TransFlat($A, $B, $data, $F = ''): array
    {
        $new = [];
        foreach ($data as $key => $value) {
            if (empty($F) === false) {
                if (array_key_exists($key, $F)) {
                    $fn = $F[$key];
                    $value = self::changeValue($value, $fn);
                }
            }
            $new[preg_replace($A, $B, $key)] = $value;
        }
        return $new;
    }

    /**
     *
     * @param type $value
     * @param type $case
     * @return mixed
     */
    private static function changeValue($value, $case)
    {
        switch ($case) {
            case 'yesno':
                return self::_yesno($value);
                break;
            case 'doendate':
                return self::_doendate($value);
                break;
            case 'doiso8601date':
                return self::_doISO8601date($value);
                break;
            case 'comp_special':
                return self::_compSpecial($value);
                break;
            case 'true_false':
                return self::_truefalse($value);
                break;
            case 'dodouble':
                return self::_dodouble($value);
                break;
            default:
                // @TODO: throw an Exception
                return $value;
        }
    }

    /**
     *
     * @param string $value
     * @return string
     */
    private static function _dodouble(string $value)
    {
        if (empty($value) === false) {
            return doubleval($value);
        }
        return '';
    }

    /**
     *
     * @param string $value
     * @return string
     */
    private static function _doendate($value)
    {
        if (empty($value) === false) {
            $d = new DateTime($value);
            return $d->format('Y-m-d');
        }
        return $value;
    }

    /**
     *
     * @param string $value
     * @return string
     */
    private static function _doISO8601date(string $value): string
    {
        if (empty($value) === false) {
            $d = new DateTime($value);
            return $d->format('c');
        }
        return $value;
    }

    /**
     *
     * @param type $value
     * @return string
     */
    private static function _yesno($value): string
    {
        if (empty($value) === false && is_bool($value)) {
            return $value === false ? 'no' : 'yes';
        }
        return (string) $value;
    }

    /**
     *
     * @param type $value
     * @return string
     */
    private static function _truefalse($value): string
    {
        if (empty($value) === false) {
            return ($value === true) ? "true" : "false";
        }
        return "";
    }

    /**
     *
     * @param type $value
     * @return string
     */
    private static function _compSpecial($value): int
    {
        if (empty($value) === false && strtolower($value) == 'comp') {
            return (int) 1;
        }
        return (int) 0;
    }
}
