<?php


namespace Zoho\Exceptions;

use Throwable;

class RefreshTokenNotFound extends \Exception implements \Throwable
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Requested Token "%s" not found', $message), $code, $previous);
    }
}
