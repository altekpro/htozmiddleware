<?php


namespace Happsnow\Exceptions;

use Throwable;

class UnauthorizedException extends \Exception implements \Throwable
{
    public function __construct($message = "Unauthorized", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
