<?php


namespace Happsnow\Exceptions;

use Throwable;

class RefreshTokenMissing extends \Exception implements \Throwable
{
    public function __construct($message = 'Generate refresh token first. Use "run.php generateToken [grandCode]"', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
