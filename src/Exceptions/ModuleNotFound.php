<?php


namespace Happsnow\Exceptions;

use Throwable;

class ModuleNotFound extends \Exception implements \Throwable
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Requested module "%s" not found', $message), $code, $previous);
    }
}
