<?php

namespace SimpleCache;

/**
 * Class SimpleCache
 * Class provides simple key=>value file system storage
 * @author Josef Šíma [josef.sima@altekpro.cz]
 * @package SimpleCache
 */
class SimpleCache
{

    /**
     * directory for store cache files
     * @var string
     */
    protected string $cacheDirectory;

    /**
     * SimpleCache constructor.
     * @param string $cacheDirectory
     */
    public function __construct(string $cacheDirectory)
    {
        $this->cacheDirectory = __ROOT__ . $cacheDirectory;
    }

    /**
     * @param string $key
     * @param $value
     * @param bool $serialize
     */
    public function put(string $key, $value, bool $serialize = false)
    {
        if ($serialize && (is_array($value) || is_object($value))) {
            $value = base64_encode(
                json_encode($value)
            );
        }

        file_put_contents($this->getFileName($key), $value);
    }

    /**
     * @param string $key
     * @param bool $deserialize
     * @param bool $assoc
     * @return false|mixed|string|null
     */
    public function get(string $key, bool $deserialize = false, $assoc = true)
    {
        $file = $this->getFileName($key);

        if (file_exists($file)) {
            $value = file_get_contents($file);
            if ($deserialize) {
                $value = json_decode(base64_decode($value), $assoc);
            }
            return $value;
        }

        return null;
    }

    /**
     * generate cache file name
     * @param $key
     * @return string
     */
    private function getFileName($key) : string
    {
        return sprintf('%s/%s.cache', $this->cacheDirectory, $key);
    }
}
